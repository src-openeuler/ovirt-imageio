Name:      ovirt-imageio
Version:   2.1.1
Release:   2
Summary:   oVirt imageio

License:   GPLv2+
Url:       https://gerrit.ovirt.org/%{name}
Source0:   http://resources.ovirt.org/pub/ovirt-master-snapshot/src/%{name}/%{name}-%{version}.tar.gz

%global ovirtimg_user ovirtimg
%global srcname ovirt_imageio
%global logdir %{_localstatedir}/log/%{name}
%global admin_confdir %{_sysconfdir}/%{name}
%global vendor_confdir %{_prefix}/lib/%{name}

%description
Image data transfer on oVirt virtualization hosts.

%prep
%setup -q

%build
%py3_build

%install
%py3_install
install -D -m 0755 --directory %{buildroot}%{logdir}
# Create a dummy log file to make rpm happy during build
touch %{buildroot}%{logdir}/daemon.log
install -D -m 0755 --directory %{buildroot}%{vendor_confdir}/conf.d
install -D -m 0755 --directory %{buildroot}%{admin_confdir}/conf.d
install -D -m 0644 data/README %{buildroot}%{admin_confdir}
install -D -m 0644 data/%{name}.service %{buildroot}%{_unitdir}/%{name}.service

%clean
rm -rf $RPM_BUILD_ROOT


%package common
Summary:   oVirt imageio common resources

# NOTE: keep in sync with automation/build-artifacts.packages
BuildRequires: gcc
BuildRequires: python3-devel

# NOTE: keep in sync with automation/check.packages
Requires:  python3

%description common
Common resources used by oVirt imageio server and client

%files common
%license COPYING
%{python3_sitearch}/%{srcname}
%{python3_sitearch}/%{srcname}-*.egg-info
%exclude %{python3_sitearch}/%{srcname}/client


%package client
Summary:   oVirt imageio client library

# NOTE: keep in sync with automation/check.packages
Requires:  %{name}-common = %{version}-%{release}
Requires:  qemu-img

%description client
Python client library for accessing imageio server on oVirt hosts.

%files client
%{python3_sitearch}/%{srcname}/client


%package daemon
Summary:   oVirt imageio daemon

# NOTE: keep in sync with automation/build-artifacts.packages
BuildRequires: systemd

# NOTE: keep in sync with automation/check.packages
Requires:  %{name}-common = %{version}-%{release}
Requires:  python3-systemd
Requires:  shadow

%description daemon
Daemon providing image transfer service on oVirt hosts.

%files daemon
%{_bindir}/%{name}
%{_unitdir}/%{name}.service
%dir %{admin_confdir}
%dir %{admin_confdir}/conf.d
%dir %{vendor_confdir}
%dir %{vendor_confdir}/conf.d
%{admin_confdir}/README
# The log directory should belong to the daemon so it can create log files.
# http://rpm.org/max-rpm-snapshot/s1-rpm-inside-files-list-directives.html#S3-RPM-INSIDE-FLIST-ATTR-DIRECTIVE
%dir %attr(755, %{ovirtimg_user}, %{ovirtimg_user}) %{logdir}
# The log files belongs to the package, so they will be removed with the package.
# http://rpm.org/max-rpm-snapshot/s1-rpm-inside-files-list-directives.html#S3-RPM-INSIDE-FLIST-GHOST-DIRECTIVE
%ghost %attr(644, %{ovirtimg_user}, %{ovirtimg_user}) %{logdir}/daemon.log*

# For more information about the systemd macros, see:
# https://fedoraproject.org/wiki/Packaging:Scriptlets#New_Packages
# For info on ordering of the scriplets, see:
# https://docs.fedoraproject.org/en-US/packaging-guidelines/Scriptlets/#ordering

%pre daemon
# Create a user and group if needed
if ! /usr/bin/getent passwd %{ovirtimg_user} >/dev/null; then
    /usr/sbin/useradd --system \
        --user-group \
        --shell /sbin/nologin \
        --home-dir /run/%{name} \
        --comment "oVirt imageio" \
        %{ovirtimg_user}
fi

%post daemon
# After installation, synchronize service state with preset files.
%systemd_post %{name}.service

%preun daemon
# Before uninstalling, stop and disable the service.
%systemd_preun %{name}.service

%postun daemon
# After upgrading, restart the service.
%systemd_postun_with_restart %{name}.service

%posttrans daemon
# At the end of the transaction, stop stale ovirt-imageio-daemon service.
# Needed only when upgrading from ovirt-imageio-daemon < 2.
if systemctl is-active ovirt-imageio-daemon.service >/dev/null; then
    echo "Stopping ovirt-imageio-daemon.service";
    systemctl stop ovirt-imageio-daemon.service
fi

%changelog
* Fri Sep 17 2021 wutao <wutao61@huawei.com> - 2.1.1-2
- add shadow requires to solve install daemon failed

* Tue Jul 6 2021 chenjian <chenjian@kylinos.cn> 2.1.1-1
- Init package
